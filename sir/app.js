/***********************config 全局变量******************************** */
require('./config/config')
//console.log("全局变量:",global.config.security.secretKey)
require('./config/logConfig')

/***********************server******************************** */
const port = 4001;

const express = require("express");
const expressWs=require("express-ws")
const app = express();

//注入express 到 express-ws中
expressWs(app);

app.use(express.json());

//操作路由，routers/index.js
const routes = require("./routers");
routes(app);

const staticRootDir ="public";
// 设置静态文件访问, public/test.png, 访问地址：http://localhost:4001/test.png
app.use(express.static(staticRootDir));

//文件树结构浏览
const serveDirectory = require('serve-directory');
app.use("/"+staticRootDir,express.static(staticRootDir));
// 配置目录浏览
app.use("/"+staticRootDir,serveDirectory(""));

app.listen(port, () => {
    console.log('npm run dev')
    console.log(`express server listening at ws://localhost:${port}/socket/wb`);
    //console.log(`express server listening at wss://game.erho.top:${port}/socket/wb`);
    console.log(`express server listening at wss://game.erho.top/socket/wb`);
    console.log('express server listening at http://localhost:' + port);
    console.log('express server listening at https://game.erho.top:' + port);
    console.log("http & websocket 同用一个端口"+port);
});


//udp
require("./routers/udp/udp_server")