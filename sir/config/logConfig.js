var log4js = require('log4js');

log4js.configure({
    appenders: {
      console: { type: 'console' },
      file: {
        type: 'file',
        filename: 'public/logs/default',
        pattern: 'yyyy-MM-dd.log',
        alwaysIncludePattern: true
      },
      errorFile: {
        type: 'file',
        filename: 'public/logs/error',
        pattern: 'yyyy-MM-dd.log',
        alwaysIncludePattern: true
      }
    },
    categories: {
      default: { appenders: ['console', 'file'], level: 'debug' },
      error: { appenders: ['errorFile'], level: 'error' }
    }
  });

var logger = log4js.getLogger();
logger.level = 'debug';
var errorLogger = log4js.getLogger('error'); // 错误日志器
/*
logger.log("Some debug messages");
logger.error("Some debug messages 1");
errorLogger.error("这是一个错误日志");
*/

// 保存原始的 console.log 方法
const originalConsoleLog = console.log;

// 重写 console.log 方法
console.log = log;
console.warn = log;
console.info =log;
console.error = error;

function log(...args)
{
  // 执行原始的 console.log 方法
  // originalConsoleLog.apply(console, args);
  logger.info(...args);
}

function error(...args)
{
    errorLogger.error(...args);
   logger.error(...args);
}


  // 测试
// console.log('This is a log message.');
// console.error('This is a log message.');
