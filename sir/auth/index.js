const jwt = require('jsonwebtoken');

const secretKey = global.config.security.secretKey
const expiresIn = global.config.security.expiresIn
const ignoreTable = global.config.ignoreTable

/**
 * 生成 token
 * @param {用户 id} uid 
 * @param {*} scope 
 */
const generateToken = function (uid, scope) {
	// 配置项中的密钥和过期时间
    
    
    // 接收三个参数，第一个是载荷，用于编码后存储在 token 中的数据，也是验证 token 后可以拿到的数据；
    // 第二个是密钥，自己定义的，验证的时候也是要相同的密钥才能解码；
    // 第三个是options，可以设置 token 的过期时间。
    const token = jwt.sign({
        uid,
        scope
    }, secretKey, {
        expiresIn: expiresIn
    })
    return token
}

/**
 * 认证
 */
function authenticate(req, res, next) {
    console.log("authenticate",req.originalUrl)

    for (let index = 0; index < ignoreTable.length; index++) {
      var element = ignoreTable[index];
      if(req.originalUrl.startsWith(element))
      {
          return next()
      }      
    }

    const token = req.headers.authorization;
    console.log("request token:",token)
    console.log("secretKey:",secretKey)
    if (!token) {
      return res.status(401).json({ message: 'Unauthorized,未经授权' });
    }
  
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.status(401).json({ message: 'Invalid token. '+ err });
      }
  
      // 验证通过，将用户信息存储在 req 中以供后续使用
      req.user = decoded;
      next();
    });
  }


const express = require("express");
var router = express.Router();

router.get('/get_token', (req, res) => {
    // 用户已通过身份验证，可以访问受保护的资源
   // console.log(req.query)
    let user_info= req.query
    let token = generateToken(user_info.user,user_info.user2)

    const user = {
      id: user_info.user,
      username: user_info.user2,
      token:token
    };

    res.header('Authorization', token).send(user);
   // res.json(token);
  });


  router.get('/test', authenticate, (req, res) => {
    // 用户已通过身份验证，可以访问受保护的资源
    res.json({ message: '测试jwt token 通过' });
  });

  // module.exports = router
  // exports.check = authenticate

  module.exports = {
    checkToken  : authenticate,
    router : router,
  }