const express = require("express");
const fs = require('fs');
var router = express.Router();


router.get("/", (req, res) => {
   
    fs.readFile('public/htmls/index.html', 'utf8',(err, data) => {
        if (err) {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          res.end('Internal Server Error');
        } else {
          res.writeHead(200, { 'Content-Type': 'text/html' });
          res.end(data);
        }
      });
  //  res.send("欢迎来到我的页面");
});

module.exports = router;