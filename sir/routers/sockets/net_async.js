
var share= require("../../share")
/**
 * send other all，排除发送者
 * @param {发送者id} id 
 * @param {*} data 
 * @param {*} msg 
 */
exports.oall=function(id, data, msg) {
 // console.log("send_other_all",id,data)
  let players=  share.findPlayers(id)
//  console.log("send_other_all",players,players.length)
  if(players!=null)
  {
    for(let i=0;i<players.length;i++)
    {
      if (players[i] != id) {
          share.send_msg(players[i], msg)
      }
    }
  }
}

/**
 * send all
 * @param {发送者id} id 
 * @param {数据包} data 
 * @param {json 字符串内容} msg 
 */
exports.all= function (id, data, msg) {
  let players=   share.findPlayers(id)
  if(players!=null)
  {
    for(let i=0;i<players.length;i++)
    {
      share.send_msg(players[i], msg)
    }
  }
}

/**
 * send other
 * @param {发送者id} id 
 * @param {userid:接受者id,其他数据} data 
 * @param {json 字符串内容} msg 
 */
exports.other= function (id, data, msg) {
    
    share.send_msg(data.userid, msg)
}