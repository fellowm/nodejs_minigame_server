const http = require("./http")
const socket = require("./socket")

const jwtToken = require("../auth")
const pages = require("../pages")

//数据转发
// http://localhost:4001/api/datatables?fields=content
const { createProxyMiddleware } = require('http-proxy-middleware');
// 创建代理中间件
const apiProxy = createProxyMiddleware('/api', {
    target: 'https://cms.erho.top/',  // 目标服务器或URL
    changeOrigin: true,  // 更改请求头中的"Host"字段
  });

module.exports = app => {

  //socket 要放在checkToken 之前注入，否者会被token 验证拦截
  app.use('/api', apiProxy);  // 数据转发   
  app.use("/socket", socket);

  app.use("/",pages)
  app.use("/cmd", http);   
  //使用JWT验证中间件,它会拦截后面的请求，做token 验证。  
  app.use(jwtToken.checkToken); 
  app.use("/auth",jwtToken.router)
 
}