//git
const gitCmd =require("./git_server")

//udp
const dgram = require('dgram');

// 创建UDP服务器
const server = dgram.createSocket('udp4');

// 监听消息事件
server.on('message', (msg, rinfo) => {
  //console.log(`接收到来自 ${rinfo.address}:${rinfo.port} 的消息: ${msg}`);
  //回复客户端 

  var arr= msg.toString().split("shell/")
  if(arr.length>1)
  {
      var cmd = arr[1];         
      // 发送消息体到内部HTTP接口
      RequestHttp(cmd,rinfo);
  }
  else if(msg.toString().startsWith("git "))
  {
    gitCmd(msg.toString(),toClinet,rinfo)
  }
  else
  {
    toClinet(rinfo,"收到");
  } 
});

// 发送数据
function toClinet(rinfo,responseMessage)
{
  const responseBuffer = Buffer.from(responseMessage);
  server.send(responseBuffer, 0, responseBuffer.length, rinfo.port, rinfo.address, (err) => {
    if (err) {
     // console.log(`回复消息失败：${err}`);
    } else {
     // console.log('已回复消息给客户端');
    }
  });
}


// 监听错误事件
server.on('error', (err) => {
  console.log(`服务器错误： ${err.stack}`);
  server.close();
});

// 监听服务器已绑定事件
server.on('listening', () => {
  const address = server.address();
  console.log(`UDP服务器已启动，正在监听 ${address.address}:${address.port}`);
});

// 绑定服务器到特定地址和端口
server.bind(4002); // 指定要监听的端口

// 关闭服务器
// server.close();



/**、
 * 内部 http 接口
 */

const http = require('http');  
const httpEndpoint = 'http://localhost:4001';

function RequestHttp(cmd,rinfo)
{
  console.log("udp RequestHttp",cmd);  
     // 调用内部 HTTP 接口
  const url = `${httpEndpoint}/${cmd}`;
  
  http.get(url, (res) => {
    let data = '';
    res.on('data', (chunk) => {
      data += chunk;
    });

    res.on('end', () => {
    //  console.log('收到内部 HTTP 接口的响应:', data);
      toClinet(rinfo,data)
    });
  }).on('error', (error) => {
    toClinet(rinfo,error)
    //console.error('调用内部 HTTP 接口时发生错误:', error);
  });
}