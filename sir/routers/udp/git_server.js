const { exec } = require('child_process');

// 调用 Git 命令
function gitCmd(command,udpback,udpInfo) {
   /* exec(`git ${command}`, (error, stdout, stderr) => {*/
    exec(`${command}`, (error, stdout, stderr) => {
      if (error) {
        console.error(`执行 Git 命令时出错: ${error}`);
        udpback(udpInfo,`执行 Git 命令时出错: ${error}`)
        return;
      }
  
      console.log('Git 命令输出:');
      console.log(stdout);
      
      udpback(udpInfo,'Git 命令输出:')
      udpback(udpInfo,stdout)
    });
}

module.exports =gitCmd