const express = require("express");
//使用中间差距，解析formdata
const multipart = require('connect-multiparty');
const multipartyMiddleware = multipart();

var router = express.Router();

//https://blog.csdn.net/y5946/article/details/89605151
//formdata

//http://localhost:4001/cmd/
router.get("/hello", (req, res) => {
   
    res.send("hello world_1");
});

router.post("/",multipartyMiddleware, async (req, res) => {
   console.log("收到请求", req.params,req.body,req.body.cmd);
    let cmdarr = req.body.cmd.split("."); 
    if(cmdarr.length==2)
    {
        try {
            let code=  require(`./http/web_${cmdarr[0]}`)
            await eval(`code.${cmdarr[1]}(req,res)`)
            return;
        } catch (error) {
            console.log("收到请求",error);
            res.json({
                code: 404,
                msg: `${req.body.cmd} :${error.message}`,
            })
        }
    }
    else
    {
        res.json({
            code: 404,
            msg: `${req.body.cmd} cmd 错误`,
        })
    }
    res.status(404).send();
});

router.put("/:id", (req, res) => {

    console.log("收到的参数id:", req.params.id);
    console.log("收到的请求体", req.body);
    res.send();
});

router.delete("/:id", (req, res) => {

    console.log("收到的参数id:", req.params.id);
    console.log("收到的请求体", req.body);
    res.status(204).send();
});

module.exports = router;