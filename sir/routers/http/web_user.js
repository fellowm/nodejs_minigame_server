const query = require("../../sql/mysql_module")
let tab_name = 'user'

//注册
exports.regist = async(req,res)=>{

    let account= req.body.account
    let pwd= req.body.pwd
    let name= req.body.name
    if(account.length<4 || account.length>12)
    {
        res.json({
            code:1001,
            msg:"帐号长度不能小于4个字符,并且不能大于12个字符"
        })
    }
    else if(pwd.length<6|| pwd.length>12)
    {
        res.json({
            code:1002,
            msg:"密码长度不能小于6个字符,并且不能大于12个字符"
        })
    }
    else if(name.length<6|| name.length>12)
    {
        res.json({
            code:1003,
            msg:"名称长度不能小于1个字符,并且不能大于12个字符"
        })
    }
    else
    {
        let sql = `SELECT * FROM ${tab_name} where account = '${req.body.account}'`
        let rows = await query(sql)
        if(rows.length>0)
        {
            res.json({
                code:1004,
                msg:"注册失败,账户已经存在"
            })
        }
        else
        {
            let data = {
                account: account,
                pwd: pwd,
                name:name
              };
              
            let sql = `INSERT IGNORE INTO ${tab_name} SET ?`;
              
            let reslut=  await query(sql, data);
             console.log(reslut.insertId,reslut.message,reslut.warningCount)
            if(reslut.warningCount==0) 
            {
                res.json({
                    code:2000,
                    msg:"注册成功"
                })
            }
            else
            {
                res.json({
                    code:1005,
                    msg:"注册失败,请检测帐号密码"
                })
            }
        }
     
    }

    res.status(200).send();
}

//注销
exports.unregist = async(req,res)=>{
    let sql = `SELECT * FROM ${tab_name} where account = '${req.body.account}' and pwd=${req.body.pwd}`
    let rows = await query(sql)
    if(rows.length>0)
    {
        sql =`DELETE FROM ${tab_name}  where account = '${req.body.account}' and pwd=${req.body.pwd}`
        let reslut = await query(sql)
        if(reslut.warningCount==0) 
        {
            res.json({
                code:1003,
                msg:"注销成功",
                data: {
                    id:reslut.insertId
                }
            })
        }
        else
        {
            res.json({
                code:1004,
                msg:"注销失败,请检测帐号密码"
            })
        }
    }
    else
    {
        res.json({
            code:1005,
            msg:"注销失败,帐号不存在."
        })
    }
    res.send()
}

//异步 登录
exports.login =async (req, res)=>
{    
    let sql = `SELECT * FROM ${tab_name} where account = '${req.body.account}' and pwd=${req.body.pwd}`
    let rows = await query(sql)
    if(rows.length>0)
    {
        res.json({
            code: 200,
            msg: '登陆成功',
            data: rows
        })
    }
    else
    {
        res.json({
            code: 404,
            msg: '登陆失败'
        })
    }

    res.status(201).send();
}

exports.login1 =async function(req, res)
{    
    let sql = `SELECT * FROM ${tab_name} where account = '${req.body.account}' and pwd=${req.body.pwd}`
    let rows = await query(sql)
    if(rows.length>0)
    {
        res.json({
            code: 200,
            msg: '登陆成功',
            data: rows
        })
    }
    else
    {
        res.json({
            code: 404,
            msg: '登陆失败'
        })
    }

    res.status(201).send();
}


exports.getXp = async function(req,res)
{
    let sql = `SELECT * FROM ${tab_name} where id = ${req.body.id}`
    let rows = await query(sql)
    if(rows.length>0)
    {
        res.json({
            code: 200,
            data: {

                xp:100
            }
        })
    }
    else
    {
        res.json({
            code: 404,
            msg: 'id 错误'
        })
    }

    res.status(201).send();
}