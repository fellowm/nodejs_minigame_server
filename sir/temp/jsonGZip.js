const zlib = require('zlib');

const jsonData = { 
    cmd:"user.cmd",
    data:{
        key1:"124545",
        key2:20,
        key3:50
    }
 };
const jsonString = JSON.stringify(jsonData);

console.log(jsonString)
zlib.gzip(jsonString, (error, compressedData) => {
  if (error) {
    console.error('压缩出错：', error);
    return;
  }

  console.log('压缩前大小：', jsonString.length);
  console.log('压缩后大小：', compressedData.length);
});


var searchString = 'user.cmd';
var replacementString = '123456';
var modifiedString = jsonString.replace(searchString, replacementString);
console.log(modifiedString); // 输出:

searchString = '123456';
replacementString= 'user.cmd';
modifiedString = modifiedString.replace(searchString, replacementString);
console.log(modifiedString); // 输出: