const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

// 自定义的JWT验证中间件
function authenticateJWT(req, res, next) {
  
    console.log(req.query,req.originalUrl)
    if(req.query,req.originalUrl.startsWith("/protected"))
    {
        const token = req.headers.authorization;


        if (token) {
          jwt.verify(token, 'your_secret_key', (err, decoded) => {
            if (err) {
              return res.sendStatus(401); // 验证失败
            }
            req.user = decoded; // 将解码后的用户信息存储在req.user中
            next(); // 验证通过，继续下一个处理函数
          });
        } else {
          res.sendStatus(401); // 缺少令牌
        }
    }
    else
    {
        console.log("通过")
        next(); 
        return;
    }
   
}

// 使用JWT验证中间件
app.use(authenticateJWT);

// 受保护的路由
app.get('/protected', (req, res) => {
  res.send('受保护的路由');
});

// 其他路由
app.get('/', (req, res) => {
  res.send('其他路由');
});

// 启动服务器
app.listen(3000, () => {
  console.log('服务器已启动');
});