const json = '{"cmd":"hello.test", "userid": "123", "msg": "hello"}';

function myFunction(a,{ cmd, userid, msg }) {
    // 在函数中使用解构后的参数
    console.log(a,cmd, userid, msg);
  }
  
  const obj = JSON.parse(json);
  myFunction(1,obj);