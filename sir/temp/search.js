const directoryPath = 'C:/Program Files/Unity/Hub/Editor/2022.1.16f1c1/Editor/Data/PlaybackEngines/WebGLSupport'; // 替换为你的目录路径
const searchString ="The error was:";//"DISABLE_EXCEPTION_CATCHING";//"The error was:";//"ABORT_ON_WASM_EXCEPTIONS";//"memory access"; //'RuntimeError';



const fs = require('fs');
const path = require('path');

function searchFiles(dir, searchString) {
  const files = fs.readdirSync(dir);

  files.forEach(file => {
    const filePath = path.join(dir, file);
    const stat = fs.statSync(filePath);

    if (stat.isDirectory()) {
      searchFiles(filePath, searchString); // 递归搜索子目录
    } else {
        var ex =path.extname(filePath);
      if (ex === '.js' || ex === '.jslib') {
        const fileContent = fs.readFileSync(filePath, 'utf8');

        if (fileContent.includes(searchString)) {
          console.log('文件包含指定字符串:',searchString, filePath);
        }
      }
    }
  });
}


searchFiles(directoryPath, searchString);