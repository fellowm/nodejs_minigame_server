var originalString = {cmd:"hello.test"};
originalString = JSON.stringify(originalString);
const encodedString = Buffer.from(originalString).toString('base64');

console.log('原始数据长度：', originalString.length);
console.log('Base64 编码后数据长度：', encodedString.length);


const decimalNumber = 42; // 要转换的整数

const binaryString = decimalNumber.toString(2);

console.log(binaryString);