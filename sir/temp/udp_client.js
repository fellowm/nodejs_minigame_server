const dgram = require('dgram');

const client = dgram.createSocket('udp4');
const serverPort = 4002;
let serverAddress = '127.0.0.1'//'43.143.11.174';
serverAddress = '43.143.11.174';
// 监听消息事件
client.on('message', (msg, rinfo) => {
  console.log(`接收到来自服务器的消息: ${msg}`);
});

// 监听错误事件
client.on('error', (err) => {
  console.log(`客户端错误： ${err.stack}`);
  client.close();
});

// 监听用户输入
process.stdin.on('data', (data) => {
  const input = data.toString().trim(); // 去除输入中的换行符和空格
  const message = Buffer.from(input);
  if(message.length>0)
  {
    // 发送消息到服务器
    client.send(message, 0, message.length, serverPort, serverAddress, (err) => {
      if (err) {
        console.log(`发送消息失败：${err}`);
        client.close();
      } else {
        console.log(`已发送消息到服务器: ${input}`);
      }
    });
  }
});

// 关闭客户端
// client.close();