const axios = require('axios');

//get
axios.get('https://game.erho.top/cmd')
  .then((response) => {
    console.log(response.data); // 处理响应数据
  })
  .catch((error) => {
    console.error(error); // 处理请求错误
  });

//post
const data = {
  key1: 'value1',
  key2: 'value2'
};

// axios.post('https://api.example.com/endpoint', data)
//   .then((response) => {
//     console.log(response.data); // 处理响应数据
//   })
//   .catch((error) => {
//     console.error(error); // 处理请求错误
//   });  