
## 安装nodejs 和npm 环境

## nodejs 项目 创建


### 1. 进入创建项目的目录, 安装依赖框架 express和 express-ws

```
npm init -y
```

执行完成后，你会发只有一个package.json 文件.

![](_assets/Pasted%20image%2020231016131120.png)

### 2. 安装 express 和 express-ws

```
npm install express
npm install express-ws
```

![](_assets/Pasted%20image%2020231016131554.png)


## 项目创建

### 1. 创建 app.js


```
const port = 4001;
const express = require("express");
const expressWs=require("express-ws")
const app = express();
expressWs(app);//注入express 到 express-ws中
app.use(express.json());
//操作路由，routers/index.js, routers 文件也需要手动创建，也可以使用命令，需要自己查询资料
//const routes = require("./routers");
//routes(app); 

app.listen(port, () => {
    console.log('npm run dev')
    console.log(`express server listening at ws://localhost:${port}/socket/wb`);
   // console.log(`express server listening at wss://node.erho.top:${port}/socket/wb`);
   console.log(`express server listening at wss://node.erho.top/socket/wb`);
    console.log('express server listening at http://localhost:' + port);
    console.log('express server listening at https://node.erho.top:' + port);
    console.log("http & websocket 同用一个端口"+port);
});
```


![](_assets/Pasted%20image%2020231016131923.png)

### 2.设置启动命令

这部分在package.json 手动完成

![](_assets/Pasted%20image%2020231016132101.png)

### 3. 运行 npm run dev

在浏览器中输入： http://localhost:4001/  
![](_assets/Pasted%20image%2020231016132224.png)

基本设置完成