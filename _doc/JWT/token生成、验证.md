### 参考：
https://blog.csdn.net/qq_42880714/article/details/132959711



### jwt

什么是JWT
JWT 是一个紧凑且独立的 JSON 对象，其中包含有关用户身份验证的信息。该信息使用密钥进行数字签名，可以被验证和信任。

JWT 由以下几方面组成：

header（标头）：包含有关 JWT 编码方式的信息，例如用于签署令牌的算法。

payload（有效负载）：这包含声明。声明是关于实体（通常是用户）和附加元数据的声明。声明分为三种类型：注册声明、公共声明和私人声明。

signature（签名）：这用于验证 JWT 的发送者是否是其声称的身份，并确保消息在此发送过程中没有被更改。签名是通过获取编码标头、编码有效负载和密钥，然后使用指定算法对它们进行创建的。

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c

```

### 安装依赖包
```
npm install express jsonwebtoken
```

config.js
```
global.config = {

    security: {

      //替换为你自己的密钥

        secretKey: 'your-secret-key',

        //https://www.npmjs.com/package/jsonwebtoken
		//100ms,60，2h,3d :100毫秒、60秒....
        expiresIn: "1h"

      }

};
```

test.js
```
const jwt = require('jsonwebtoken');  
const secretKey = global.config.security.secretKey
const expiresIn = global.config.security.expiresIn  

/**
 * 生成 token
 * @param {用户 id} uid
 * @param {*} scope
 */
const generateToken = function (uid, scope) {
  // 配置项中的密钥和过期时间
    // 接收三个参数，第一个是载荷，用于编码后存储在 token 中的数据，也是验证 token 后可以拿到的数据；
    // 第二个是密钥，自己定义的，验证的时候也是要相同的密钥才能解码；
    // 第三个是options，可以设置 token 的过期时间。
    const token = jwt.sign({
        uid,
        scope
    }, secretKey, {
        expiresIn: expiresIn
    })
    return token
}

  

/**
 * 认证
 */
function authenticate(req, res, next) {
    const token = req.headers.authorization;
    console.log("request token:",token)
    console.log("secretKey:",secretKey)
    if (!token) {
      return res.status(401).json({ message: 'Unauthorized,未经授权' });
    }
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.status(401).json({ message: 'Invalid token. '+ err });
      }
      // 验证通过，将用户信息存储在 req 中以供后续使用
      req.user = decoded;
      next();
    });
  } 
  

const express = require("express");
var route = express.Router();  

  route.get('/get_token', (req, res) => {
    // 用户已通过身份验证，可以访问受保护的资源
   // console.log(req.query)
    let user_info= req.query
    let token = generateToken(user_info.user,user_info.user2)
    const user = {
      id: user_info.user,
      username: user_info.user2,
      token:token
    }; 
    res.header('Authorization', token).send(user);
   // res.json(token);
  });  

  route.get('/test', authenticate, (req, res) => {
    // 用户已通过身份验证，可以访问受保护的资源
    res.json({ message: '测试jwt token 通过' });
  });
  module.exports =route
```


### 在Apifox 中测试

#### 生成token
http://localhost:4001/auth/get_token?user=123&user2=123 获取token，


![](../../_assets/Pasted%20image%2020231017163852.png)

Authorization 授权， 可以看到token信息，生成完成

![](../../_assets/Pasted%20image%2020231017163952.png)

#### 验证token 是否有效

http://localhost:4001/auth/test
确保Authorization 授权 的token 已经加入

![](../../_assets/Pasted%20image%2020231017164223.png)
