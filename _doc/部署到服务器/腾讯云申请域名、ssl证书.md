## 1.在腾讯云中创建 game.erho.top 域名

![](../../_assets/Pasted%20image%2020231020145016.png)

## 2. 设置 ssl 证书。
https://console.cloud.tencent.com/ssl

![](../../_assets/Pasted%20image%2020231020145114.png)

申请免费证书
![](../../_assets/Pasted%20image%2020231020151156.png)

域名验证：

![](../../_assets/Pasted%20image%2020231020152315.png)

把左边的值，复制到右边添加即可。
![](../../_assets/Pasted%20image%2020231020152622.png)

然后点击“域名验证”

![](../../_assets/Pasted%20image%2020231020152746.png)

## 下载ssl 证书
![](../../_assets/Pasted%20image%2020231020153045.png)

## 4.设置防火墙 
开发端口 4001

![](../../_assets/Pasted%20image%2020231020152002.png)
