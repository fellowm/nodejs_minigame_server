请求：

```
//创建或者加入
{
    "c":"lobby.createOrjoin",
    "name":"hello_world"
}
//其他人加入


//离开
{
    "cmd":"lobby.leave"
}

```

响应：
```
//创建或者加入
{
    "c": "lobby.createOrjoin",
    "data": {
        "name": "hello_world",
        "players": [
            456，
            132
        ]
    }
}

//其他人加入
{
    "c":"lobby.otherJoin",
    "data":{
	    "id":123
    }
}

//自己离开
{
    "c": "lobby.leave",
    "data": {
        "id": 458
    }
}

//其他人离开
{
    "c": "lobby.otherLeave",
    "id": 458
}

```