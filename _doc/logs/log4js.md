
参考：https://zhuanlan.zhihu.com/p/649345365

![](../../_assets/Pasted%20image%2020231019123633.png)

![](../../_assets/Pasted%20image%2020231019123650.png)
## 安装
```
npm install log4js
```


### 代码

config/logConfig.js

```
var log4js = require('log4js');

log4js.configure({
    appenders: {
      console: { type: 'console' },
      file: {
        type: 'file',
        filename: 'public/logs/default',
        pattern: 'yyyy-MM-dd.log',
        alwaysIncludePattern: true
      },
      errorFile: {
        type: 'file',
        filename: 'public/logs/error',
        pattern: 'yyyy-MM-dd.log',
        alwaysIncludePattern: true
      }
    },
    categories: {
      default: { appenders: ['console', 'file'], level: 'debug' },
      error: { appenders: ['errorFile'], level: 'error' }
    }
  });

var logger = log4js.getLogger();
logger.level = 'debug';
var errorLogger = log4js.getLogger('error'); // 错误日志器
/*
logger.log("Some debug messages");
logger.error("Some debug messages 1");
errorLogger.error("这是一个错误日志");
*/

// 保存原始的 console.log 方法
const originalConsoleLog = console.log;

// 重写 console.log 方法
console.log = log;
console.warn = log;
console.info =log;
console.error = error;

function log(...args)
{
  // 执行原始的 console.log 方法
  // originalConsoleLog.apply(console, args);
  logger.info(...args);
}

function error(...args)
{
    errorLogger.error(...args);
   logger.error(...args);
}


  // 测试
// console.log('This is a log message.');
// console.error('This is a log message.');


```

在app.js 或者server.js 中导入模块，全局就可以使用了
require('./config/logConfig')
### 捕获系统console

```
// 重写 console.log 方法
console.log = function (...args) {
  // 执行原始的 console.log 方法
 // originalConsoleLog.apply(console, args);

  // 添加自定义逻辑，将日志消息记录到 log4js
  logger.info(...args);
};
```