http-proxy-middleware： 中间插件，可以做http数据转发
http请求：
 http://localhost:4001/api/datatables?fields=content
 目标：
 https://cms.erho.top/api/datatables?fields=content

```
//数据转发

// http://localhost:4001/api/datatables?fields=content

const { createProxyMiddleware } = require('http-proxy-middleware');

// 创建代理中间件

const apiProxy = createProxyMiddleware('/api', {

    target: 'https://cms.erho.top/',  // 目标服务器或URL

    changeOrigin: true,  // 更改请求头中的"Host"字段

  });

  app.use('/api', apiProxy);  // 数据转发
```

![](../../_assets/Pasted%20image%2020231016183332.png)