## json 数据反序列化成对象，对象参数顺序不是不确定的，如何对应到函数的实参中

如果你有一个 JSON 数据，要将其反序列化为对象，并且对象参数的顺序不确定，但你需要将它们对应到函数的实参中，可以考虑使用对象解构赋值和默认参数来实现。

假设你有以下 JSON 数据：

```
const json = '{"param2": "value2", "param1": "value1", "param3": "value3"}';
```

你可以将其反序列化为对象，并在函数调用时使用对象解构赋值和默认参数。

```
function myFunction({ param1, param2, param3 }) {
  // 在函数中使用解构后的参数
  console.log(param1, param2, param3);
}

const obj = JSON.parse(json);
myFunction(obj);
```


在这个例子中，无论 JSON 数据中的参数顺序如何，对象解构赋值会自动将对应的属性值分配给函数的实参。如果 JSON 数据中缺少某个属性，可以在函数参数中设置默认值，以确保代码的健壮性。

这样，无论对象参数的顺序如何，都能正确地将其对应到函数的实参中



```
const json = '{"cmd":"hello.test", "userid": "123", "msg": "hello"}'; 
function myFunction(a,{ cmd, userid, msg }) {
    // 在函数中使用解构后的参数
    console.log(a,cmd, userid, msg);
  }
  const obj = JSON.parse(json);
  myFunction(1,obj);
```